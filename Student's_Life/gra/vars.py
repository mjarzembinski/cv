import pygame

red = (255, 0, 0)
font_size = 23
white = (255, 255, 255)
green = (0, 255, 0)
blue = (0, 0, 128)
black = (0, 0, 0)
screen_width = 1550
screen_height = 800
display_surface = pygame.display.set_mode((screen_width, screen_height))

powitanie = '\nWitaj w symulatorze życia studenckiego!\n\
Twoim głównym zadaniem będzie zdanie sesji, ale na swojej drodze spotkasz wiele przeszkód.\n\
Nie martw się jednak, bo na pewno znajdziesz też czas na kilka szalonych imprez w akademiku i zdobędziesz wiele nowych znajomości.\n\
Powodzenia!\n\n\n Naciśnij dowolny klawisz, aby kontynuować.'

wstep = '''\n\n\n\nGłówną walutą w grze jest czas do sesji, więc używaj go mądrze, bo jest kluczowy do przejścia gry.\n\n\n\
Przed Tobą pierwszy ważny wybór.\n\n\n'''

dzien1 = ''' \n\n\n\n\n\n\n\n\n\n Dzień pierwszy\n\n\
A. Idź na imprezę do akademika - koszt: 8h, znajomości: +10, monety: -50 \n\n\
B. Zostań w domu i przygotuj się do wejściówki z programowania - koszt: 4h, wiedza: +3 \n\n\n\
Kliknij na odpowiednią literkę, żeby dokonać wyboru. '''


dzien2 = '''\n\n\n\n\n Dzień drugi \n\n
A. Wróć na weekend do domu - koszt: 6h \n
B. Zostań w mieście - koszt: -50 monet '''

dzien3 = '''\n\n\n\n\n Dzień trzeci \n\n
A. Idź do pracy dorywczej - koszt: 8h, monety: +75 \n
B. Ucz się - koszt: 6h, wiedza: +10'''

dzien4 = ''''\n\n\n\n\n Dzień czwarty \n\n
A. Napisz do prowadzącego zajęcia w sprawie grupy - koszt: 4h, znajomości: +10 \n
B. Ucz się - koszt: 4h, wiedza: +5'''

dzien5 = '''\n\n\n\n\n Dzień piąty \n\n
Czas na projekt grupowy - wymagana ilość punktów znajomości: minimum 10'''

klawisze= '''Naciśnij odpowiednie cyfry, aby kontynuować'''

meni = '\n\n\n\n1. Nowa gra \n\n 2. Najlepsze wyniki \n\n 3. Autorzy \n\n 4. Wyjdź z gry'



scena7A = '''\n\n\n\n\n\n\n\n\n\n\nBrawo, przechodzisz dalej'''
scena7B = '''\n\n\n\n\nKoniec gry \n\n\n\n\n\n\nNiestety, nikogo nie znałeś, więc nie zaliczyłeś projektu grupowego'''

game_over_bieda = '''\n\n\n\n\nKoniec gry\n\n\n\n\n\n\n\n Niestety, zabrakło Ci pieniędzy na życie, więc musisz porzucić studia i iść do pracy'''

dzien6 ='''\n\n\n\n\n\n\nDzień szósty\n\nDzisiaj kolokwium!\n\n
A. Napisz samemu - wymagany poziom wiedzy do zaliczenia: 10 \n
B. Daj łapówkę wykładowcy: -300 monet'''

dzien7 = '''\n\n\n\n\n\n\n Brawo, udało ci się wytrwać do sesji \n\n\n Zaraz zostanie obliczony Twój wynik'''

autor = '''\n\n\n\n\n Autor: Mateusz Jarzembiński \n\n Rok powstania: 2021 \n\n Uniwersytet Gdański, informatyka praktyczna,
nauczyciel prowadzący: Pani mgr inż. Anna Nenca'''

powrot_do_menu = '''\n\n\n\n\n\n\n\n\n\n\n\n\n Naciśnij jakikolwiek przycisk, aby wrócić do głównego menu'''

brawo = "\n\n\n\n\n\n\n\n\n\n\n\n Brawo, przechodzisz dalej"