from vars import *
import pygame

pygame.init()

display_surface = pygame.display.set_mode((screen_width, screen_height))

pygame.display.set_caption('Show Text')


def lines_breaker(words, font_size):
    text_list = []
    font = pygame.font.Font('freesansbold.ttf', font_size)
    for string in words.splitlines():
        dict_tmp = {"font": font.render(string, True, white, blue)}
        dict_tmp["rect"] = dict_tmp["font"].get_rect()
        dict_tmp["rect"].center = (screen_width // 2, 50 + len(text_list) * font_size)
        text_list.append(dict_tmp)
    for i in range(len(text_list)):
        display_surface.blit(text_list[i]["font"], text_list[i]["rect"])
