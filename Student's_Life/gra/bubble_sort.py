with open("results.txt", 'r') as wyniki:
    wynik_lista = eval(wyniki.read())
    wynik_lista = list(wynik_lista)


def bubble_sort():
    for x in reversed(range(len(wynik_lista) - 1)):
        for y in range(x):
            if wynik_lista[y] > wynik_lista[y + 1]:
                storage = wynik_lista[y]
                wynik_lista[y] = wynik_lista[y + 1]
                wynik_lista[y + 1] = storage
    return wynik_lista


with open ("results.txt", 'r') as wyniki:
    bubble_sort()