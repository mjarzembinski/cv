from bubble_sort import *
from center_text import *
from vars import *

pygame.init()
pygame.display.set_caption("Student's life")
pygame.mixer.music.load("jpg/yt1s.com - Pink Floyd  Interstellar Overdrive HQ.mp3")
pygame.mixer.music.set_volume(0.01)
pygame.mixer.music.play()
screen = pygame.display.set_mode((screen_width, screen_height))
font = pygame.font.Font('freesansbold.ttf', 28)
running = True
start_screen = pygame.image.load("jpg/Fotoram.io.jpg")
game_over_image = pygame.image.load("jpg/zdjatko.jpg")
you_won_image = pygame.image.load("jpg/graduation.jpg")
bieda_image = pygame.image.load("jpg/bieda.jpg")
samotnosc_image = pygame.image.load("jpg/sama.jpg")
aktualnaScena = None
font2 = pygame.font.Font('freesansbold.ttf', 28)
czyTloNarysowane = False
czy_zapisane = True

with open("results.txt", 'r') as wyniki:
    wynik_lista = wyniki.read()
    wynik_lista = list(wynik_lista)

global godziny
global wiedza
global znajomości
global monety

monety = 300
godziny = 70
znajomości = 0
wiedza = 0

def wynik():
    global godziny
    global wiedza
    global znajomości
    global monety
    wynik = "godziny do wykorzystania: " + str(godziny) + ", punkty wiedzy: " + str(wiedza) \
            + ",  punkty znajomości: " + str(znajomości) + ",  monety: " + str(monety)
    return lines_breaker(wynik, 23)


def zmienScene(nowaScena):
    screen.fill((0, 0, 0))
    global czyTloNarysowane
    global aktualnaScena
    czyTloNarysowane = False
    aktualnaScena = nowaScena


def scena1():
    global czyTloNarysowane
    global running
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        lines_breaker(klawisze, 40)
        lines_breaker(meni, 40)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_1:
                zmienScene(scena2)
            if event.key == pygame.K_2:
                zmienScene(najlepsze_wyniki)
            if event.key == pygame.K_3:
                zmienScene(autorzy)
            if event.key == pygame.K_4:
                running = False


def najlepsze_wyniki():
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        bubble_sort()
        with open("results.txt", 'r') as wyniki:
            zawartosc = wyniki.read()

            wynik_lista = zawartosc.split(",")

            for i in range(0, 5):
                text = font2.render(wynik_lista[i], True, (black))
                textRect = text.get_rect()
                textRect.center = (screen_width // 2, (screen_height * 1 // 10) + i * 25)
                screen.blit(text, textRect)

        lines_breaker(powrot_do_menu, 30)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            zmienScene(scena1)


def autorzy():
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        lines_breaker(autor, 30)
        lines_breaker(powrot_do_menu, 30)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            zmienScene(scena1)


def scena2():
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        lines_breaker(powitanie, 23)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            zmienScene(scena3)


def scena3():
    global godziny
    global wiedza
    global znajomości
    global monety
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        wynik()
        lines_breaker(wstep, 28)
        lines_breaker(dzien1, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                godziny -= 8
                znajomości += 10
                monety -= 50
                zmienScene(scena4_A)
            elif event.key == pygame.K_b:
                godziny -= 4
                wiedza += 3
                zmienScene(scena4_B)


def scena4_A():
    global godziny
    global wiedza
    global znajomości
    global monety
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        wynik()
        lines_breaker(dzien2, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                godziny -= 6
                zmienScene(scena5_A)
            elif event.key == pygame.K_b:
                monety -= 50
                zmienScene(scena5_B)


def scena4_B():
    global godziny
    global wiedza
    global znajomości
    global monety
    global running

    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        wynik()
        lines_breaker(dzien2, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                godziny -= 6
                zmienScene(scena5_A)
            elif event.key == pygame.K_b:
                monety -= 50
                zmienScene(scena5_B)


def scena5_A():
    global godziny
    global wiedza
    global znajomości
    global monety
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        wynik()
        lines_breaker(dzien3, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                godziny -= 8
                monety += 75
                zmienScene(scena6_A)
            elif event.key == pygame.K_b:
                godziny -= 6
                wiedza += 10
                zmienScene(scena6_B)


def scena5_B():
    global godziny
    global wiedza
    global znajomości
    global monety
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        wynik()
        lines_breaker(dzien3, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                godziny -= 8
                monety += 75
                zmienScene(scena6_A)
            elif event.key == pygame.K_b:
                godziny -= 6
                wiedza += 10
                zmienScene(scena6_B)


def scena6_A():
    global godziny
    global wiedza
    global znajomości
    global monety
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        wynik()
        lines_breaker(dzien4, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                godziny -= 4
                znajomości += 10
                zmienScene(scena7_A)
            elif event.key == pygame.K_b:
                godziny -= 4
                wiedza += 5
                zmienScene(scena5_B)


def scena6_B():
    global godziny
    global wiedza
    global znajomości
    global monety
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        wynik()
        lines_breaker(dzien4, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                godziny -= 4
                znajomości += 10
                zmienScene(scena7_A)
            elif event.key == pygame.K_b:
                godziny -= 4
                wiedza += 5
                zmienScene(scena7_B)


def scena7_A():
    global godziny
    global wiedza
    global znajomości
    global monety
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        wynik()
        lines_breaker(dzien5, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        else:
            pygame.time.wait(6000)
            lines_breaker(scena7A, 23)
            pygame.time.wait(6000)
            zmienScene(scena8)


def scena7_B():
    global godziny
    global wiedza
    global znajomości
    global monety
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        wynik()
        lines_breaker(dzien5, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if znajomości < 10:
            lines_breaker("\n\n\n\n\n\n\n\n\n\n\n\n\n Kliknij dowolny przycisk, aby kontynuować", 28)
            if event.type == pygame.KEYDOWN:
                zmienScene(sama)
        else:
            lines_breaker(brawo, 28)
            lines_breaker("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n Kliknij dowolny przycisk, aby kontynuować", 28)
            if event.type == pygame.KEYDOWN:
                lines_breaker(scena7A, 23)
                if event.type == pygame.KEYDOWN:
                    zmienScene(scena8)


def sama():
    global czyTloNarysowane
    global running
    if czyTloNarysowane == False:
        screen.blit(samotnosc_image, (0, 0))
        wynik()
        lines_breaker(scena7B, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            zmienScene(scena1)


def bieda():
    global czyTloNarysowane
    global running
    if czyTloNarysowane == False:
        screen.blit(bieda_image, (0, 0))
        wynik()
        lines_breaker(game_over_bieda, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            zmienScene(scena1)


def scena8():
    global godziny
    global wiedza
    global znajomości
    global monety
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        wynik()
        lines_breaker(dzien6, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_b:
                monety -= 300
                zmienScene(scena9)
        if wiedza > 9:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    zmienScene(scena9)


def scena9():
    global godziny
    global wiedza
    global znajomości
    global monety
    global running
    global czyTloNarysowane

    if czyTloNarysowane == False:
        screen.blit(start_screen, (0, 0))
        wynik()
        lines_breaker(dzien6, 28)
        czyTloNarysowane = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif monety < 0:
            zmienScene(bieda)
        else:
            lines_breaker("\n\n\n\n\n\n\n\n\n\n\n\n\n\n Kliknij dowolny przycisk, aby kontynuować", 28)
            if event.type == pygame.KEYDOWN:
                zmienScene(scena10)


def scena10():
    global czy_zapisane
    global godziny
    global wiedza
    global znajomości
    global monety
    global running
    global czyTloNarysowane
    if czyTloNarysowane == False:
        screen.blit(you_won_image, (0, 0))
        wynik()
        lines_breaker(dzien7, 28)
        czyTloNarysowane = True
    if czy_zapisane:
        with open("results.txt", 'r') as wyniki:
            wynik_lista = wyniki.read()
            wynik_lista = list(wynik_lista)
            czy_zapisane = False
    with open("results.txt", 'r') as wyniki:
        wynik_lista = wyniki.read()
        wynik_lista = list(wynik_lista)
        końcowy_wynik = (wiedza + 0.01 * monety + 0.4 * znajomości)
        wynik_lista.append(końcowy_wynik)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        else:
            lines_breaker("\n\n\n\n\n\n\n\n\ Twój wynik: " + str(końcowy_wynik), 50)
            lines_breaker(powrot_do_menu, 50)

            if event.type == pygame.KEYDOWN:
                zmienScene(scena1)


aktualnaScena = scena1

while running:
    aktualnaScena()

    pygame.display.flip()
    pygame.time.wait(16)

pygame.quit()
