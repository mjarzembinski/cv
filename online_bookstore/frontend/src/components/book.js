/* eslint-disable react/no-unknown-property */
/* eslint-disable react/prop-types */
import {Link} from 'react-router-dom';
import React  from 'react'

const Book = (props) =>{
    function cover(){
        if (props.book.image_url !== null && props.book.image_url !== ""){
            return((<img src={props.book.image_url} alt="bookCoverNotFound.jpg" class="photo"></img>))
        }
    }
    return(
        <div className="book">
            <div>
                <li>Title: {props.book.title}</li>
                <li>Genre: {props.book.genre}</li> 
                <li>Release date: {new Date(props.book.release_date).toLocaleDateString('en-CA').slice(0, 10)}</li>
                <li><Link to={"/book/" + props.book.id}>More details</Link></li>
            </div>
            <div>
                {cover()}
            </div>
            
            
        </div>
    )
}

export default Book;
