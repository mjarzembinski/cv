
import {Link} from 'react-router-dom';
import React  from 'react'

function Navbar() {
    return (
      <div id="sidebar">
          <li><Link to="/">Main page</Link></li>
          <li><Link to="/add">Add book</Link></li>
          <li><Link to="/favorite ">Favourite </Link></li>
      </div>
    );
  }

export default Navbar;