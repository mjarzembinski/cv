/* eslint-disable react/no-unknown-property */
import { Formik, Form, Field } from "formik";
import axios from "axios";
import React from "react";

function BookAdd() {
  return (
    <div className="addBook">
      <img
        src="https://i.pinimg.com/originals/ea/5a/6d/ea5a6d2ed385dc9536d7378ab9eb2b6e.jpg"
        alt="404"
        class="mainPhoto"
      ></img>
      <h2>Add your books</h2>
      <Formik
        initialValues={{
          title: "",
          author: "",
          genre: "",
          release_date: "",
          description: "",
          image_url: "",
        }}
        validate={(values) => {
          const errors = {};
          if (!values.title) {
            errors.title = "Required";
          }
          if (!values.author) {
            errors.author = "Required";
          }
          if (!values.release_date) {
            errors.release_date = "Required";
          }
          if (!values.genre) {
            errors.genre = "Required";
          }

          return errors;
        }}
        onSubmit={async (values) => {
          await axios.post("http://localhost:5000/api/book", values);
          alert("Book added!");
          window.location.reload();
        }}
        render={({ errors }) => (
          <Form>
            Title: <Field name="title" type="text" />
            <div className="error">{errors.title}</div>
            Author: <Field name="author" type="text" />
            <div className="error">{errors.author}</div>
            Genre:{" "}
            <Field name="genre" component="select">
              <option value="">-------------</option>
              <option value="classics">classics</option>
              <option value="fantasy">fantasy</option>
              <option value="history">history</option>
              <option value="horror">horror</option>
              <option value="romance">romance</option>
              <option value="science fiction">science fiction</option>
              <option value="biographies">biographies</option>
              <option value="autobiographies">autobiographies</option>
              <option value="cookbook">cookbook</option>
              <option value="others">others</option>
            </Field>
            <div className="error">{errors.genre}</div>
            Release date: <Field name="release_date" type="date" />
            <div className="error">{errors.release_date}</div>
            Description: <Field name="description" type="text" />
            Image url: <Field name="image_url" type="text" />
            <button type="submit">Submit</button>
          </Form>
        )}
      ></Formik>
    </div>
  );
}

export default BookAdd;
