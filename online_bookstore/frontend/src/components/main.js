/* eslint-disable react/no-unknown-property */
/* eslint-disable react/jsx-key */

/* eslint-disable no-undef */
import axios from "axios";
import {useEffect, useState} from "react";
import Book from "./book.js";
const _ = require('lodash');
import React  from 'react'

function Main() {
  const [book, setBook] = useState("")
  const [sortType, setSortType] = useState("id")
  const [filterType, setFilterType] = useState("")

  useEffect(()=>{
    axios.get('http://localhost:5000/api/book/')
    .then(response=>setBook(response.data))
  },[])

  const displayBooks = () =>{
    if (!filterType){
      if(sortType === "title"){
        return(
          (
            _.chain(_.sortBy(book,[sortType, 'id'])).map(x => (<Book book={x}/>)).value()
          )
        )
      }
      else{
        return(
          (
            _.chain(_.sortBy(book,[sortType])).map(x => (<Book book={x}/>)).value()
          )
        )
      }
    }
    else{
      if(sortType === "title"){
        return(
          (
            _.chain(_.sortBy(book,[sortType, 'id'])).filter(x => x.genre===filterType).map(x => (<Book book={x}/>)).value()
          )
        )
      }
      else{
        return(
          (
            _.chain(_.sortBy(book,[sortType])).filter(x => x.genre===filterType).map(x => (<Book book={x}/>)).value()
          )
        )
      }
    }
    
  }

    return (
        <div className="main">
          <img src="https://wallpaper.dog/large/17199201.png" alt="404" class="mainPhoto"></img>
          <h2>Welcome to the main page</h2>
          <div className="sortbar">
            <div>
              <label for="sort">
                Sort by:  
              </label>
              <select id="sort" onChange={(e) => setSortType(e.target.value)}>
                <option value="id">-------------</option>
                <option value="title">title</option>
                <option value="genre">genre</option>
                <option value="date">release date</option>
                <option value="author">author</option>
              </select>
              <label for="filter">
                Filter by:  
              </label>
              <select id="filter" onChange={(e) => setFilterType(e.target.value)}>
                <option value="">-------------</option>
                <option value="classics">classics</option>
                <option value="fantasy">fantasy</option>
                <option value="history">history</option>
                <option value="horror">horror</option>
                <option value="romance">romance</option>
                <option value="science fiction">science fiction</option>
                <option value="biographies">biographies</option>
                <option value="autobiographies">autobiographies</option>
                <option value="cookbook">cookbook</option>
                <option value="others">others</option>
              </select>
            </div>  
          </div>
          <div className="books">
           {displayBooks()}
          </div>
          
        </div>
    );
  }

export default Main;