/* eslint-disable react/no-unknown-property */
import React, { useState, useEffect } from "react";
import { Formik, Form, Field } from "formik";
import { useHistory } from "react-router-dom";
import axios from "axios";

function BookEdit() {
  let x = window.location.pathname.slice(6);
  let history = useHistory();

  const [book, setBook] = useState("");
  const [ifHave, setIfHave] = useState(false);

  useEffect(() => {
    axios
      .get("http://localhost:5000/api/book/" + x)
      .then((response) => setBook(response.data))
      .then(() => setIfHave(true));
  }, []);

  return (
    <div className="addBook">
      <img
        src="https://cdn.wallpapersafari.com/23/7/w90ikX.jpg"
        alt="404"
        class="mainPhoto"
      ></img>
      <h2>Edit the book</h2>
      {ifHave ? (
        <Formik
          initialValues={{
            title: book.title,
            author: book.author,
            genre: book.genre,
            release_date: new Date(book.release_date)
              .toLocaleDateString("en-CA")
              .slice(0, 10),
            description: book.description,
            image_url: book.image_url,
          }}
          validate={(values) => {
            const errors = {};
            if (!values.title) {
              errors.title = "Required";
            }
            if (!values.author) {
              errors.author = "Required";
            }
            if (!values.release_date) {
              errors.release_date = "Required";
            }
            if (!values.genre) {
              errors.genre = "Required";
            }

            return errors;
          }}
          onSubmit={async (values) => {
            await axios.put("http://localhost:5000/api/book/" + x, values);
            history.goBack();
          }}
          render={({ errors }) => (
            <Form>
              Title: <Field name="title" type="text" />
              <div className="error">{errors.title}</div>
              Author: <Field name="author" type="text" />
              <div className="error">{errors.author}</div>
              Genre:{" "}
              <Field name="genre" component="select">
                <option value="">-------------</option>
                <option value="classics">classics</option>
                <option value="fantasy">fantasy</option>
                <option value="history">history</option>
                <option value="horror">horror</option>
                <option value="romance">romance</option>
                <option value="science fiction">science fiction</option>
                <option value="biographies">biographies</option>
                <option value="autobiographies">autobiographies</option>
                <option value="cookbook">cookbook</option>
                <option value="others">others</option>
              </Field>
              <div className="error">{errors.genre}</div>
              Release date: <Field name="release_date" type="date" />
              <div className="error">{errors.release_date}</div>
              Description: <Field name="description" type="text" />
              Image url: <Field name="image_url" type="text" />
              <button type="submit">Submit</button>
            </Form>
          )}
        ></Formik>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
}

export default BookEdit;
