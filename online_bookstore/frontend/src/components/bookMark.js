/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unknown-property */

import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import React, { Component }  from 'react'

function Mark() {
  let x = window.location.pathname.slice(6);
  const [book, setBook] = useState("");
  const [, setIfHave] = useState(false);
  let history = useHistory();

  useEffect(() => {
    axios
      .get("http://localhost:5000/api/book/" + x)
      .then((response) => setBook(response.data))
      .then(() => setIfHave(true));
  }, [book.title]);

  const submit = () => {
    const mark = document.getElementsByName("mark");
    for (let i = 0; i < mark.length; i++) {
      if (mark[i].checked) {
        axios.post("http://localhost:5000/api/book/" + x + "/rate", {
          score: mark[i].value,
        });
        history.goBack();
      }
    }
  };

  return (
    <div className="bookMark">
      <img
        src="https://i.imgur.com/7bJuxdH.jpg"
        alt="404"
        class="mainPhoto"
      ></img>
      <h2>{'Mark the " ' + book.title + ' " book'}</h2>
      <div>
        <li>
          <input type="radio" id="mark-5" name="mark" value="5" />
          <label for="mark-5"> This book is amazing</label>
        </li>
        <li>
          <input type="radio" id="mark-4" name="mark" value="4" />
          <label for="mark-4"> This is a very good book</label>
        </li>
        <li>
          <input type="radio" id="mark-3" name="mark" value="3" />
          <label for="mark-3"> I like it </label>
        </li>
        <li>
          <input type="radio" id="mark-2" name="mark" value="2" />
          <label for="mark-2"> The book is not so good</label>
        </li>
        <li>
          <input type="radio" id="mark-1" name="mark" value="1" />
          <label for="mark-1"> This book is terrible</label>
        </li>
        <li>
          <button type="submit" onClick={submit}>
            Submit
          </button>
        </li>
      </div>
    </div>
  );
}

export default Mark;
