/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unknown-property */
import {useEffect, useState} from "react";
import {Link, useHistory} from 'react-router-dom';
import axios from "axios";
import React, { Component }  from 'react'


function BookDetails() {

  let x = window.location.pathname.slice(6)
  const [book, setBook] = useState("");
  const [ifHave, setIfHave] = useState(false)
  let history = useHistory();

  useEffect(()=>{
      axios.get("http://localhost:5000/api/book/" + x)
      .then(response => setBook(response.data))
      .then(() => setIfHave(true))
  }, [book.title])

  function cover(){
    if (book.image_url !== null && book.image_url !== ""){
        return((<img src={book.image_url} alt="bookCoverNotFound.jpg"></img>))
    }}
return (
  <div className="bookDetails">
    <img src="https://images.alphacoders.com/166/166721.jpg" alt="404" class="mainPhoto"></img>
    <h2>{'" ' + book.title + ' "'}</h2>
    <div className="bookCover">
      {cover()}
    </div>
    <div>
      <li>Title: {book.title}</li>
      <li>Author: {book.author}</li>
      <li>Genre: {book.genre}</li>
      <li>Release date: {ifHave ? new Date(book.release_date).toLocaleDateString('en-CA').slice(0, 10) : 'Loading...'}</li>
      <li>Rating: {Math.round(book.rating)}/5</li>
      <li>Description: {book.description} </li>
      <li><Link to={"/edit/" +book.id}>Edit</Link> <Link to={"/mark/" + book.id}>Add mark</Link> <button onClick={() => (
        axios.delete('http://localhost:5000/api/book/' + x).then(window.location.replace("http://localhost:3000/"))
      )}>DEL</button> </li>
    </div>

  </div>
)
}

export default BookDetails;
