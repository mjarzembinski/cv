import { Route, Switch } from "react-router-dom";
import Navbar from "./components/navbar.js";
import Main from "./components/main.js";
import BookAdd from "./components/bookAdd.js";
import BookDetails from "./components/bookDetails.js";
import BookEdit from "./components/bookEdit.js";
import Mark from "./components/bookMark.js";
import "./style.scss";
import React from "react";

function App() {
  return (
    <div id="body">
      <Navbar />
      <Switch>
        <Route path="/" exact>
          {" "}
          <Main />{" "}
        </Route>
        <Route path="/add/">
          {" "}
          <BookAdd />{" "}
        </Route>
        <Route path="/book/">
          {" "}
          <BookDetails />{" "}
        </Route>
        <Route path="/edit/">
          {" "}
          <BookEdit />{" "}
        </Route>
        <Route path="/mark/">
          {" "}
          <Mark />{" "}
        </Route>
      </Switch>
    </div>
  );
}

export default App;
