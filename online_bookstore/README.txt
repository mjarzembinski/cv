This is an online bookstore project. I did not coded the API, it was provided by my university teacher.
To run the project, you need to have postgres database, and node.js package
as well as installed yarn.
Then you need to enter the index.js file in BooksAPI-main and type the password and port 
you are using (in about 130-th line of code). After that go to BooksAPI-main and type 
yarn install in the terminal, then do the same in the frontend folder. Now, go again to the BooksAPI-main
and in the terminal type yarn start, do it for the frontend folder too.